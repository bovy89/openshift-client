FROM centos:7


RUN yum install -y tar && yum clean all

RUN curl -L https://github.com/openshift/origin/releases/download/v3.6.0/openshift-origin-client-tools-v3.6.0-c4dd4cf-linux-64bit.tar.gz | tar xvz -C /bin/ --strip-components=1

CMD ["/bin/oc"]


